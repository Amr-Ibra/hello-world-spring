package com.example.helloworldspring;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(HelloController.class)
public class HelloControllerTests {

  @Autowired
  MockMvc mockMvc;

  @Test
  public void sayHello_noParam_rtnHelloWorld() throws Exception {
    // /hello string "Hello World"
    // Arrange

    // Act
    this.mockMvc.perform(get("/hello"))
        // Assert
        .andExpect(status().isOk())
        .andExpect(content().string("Hello World"));
  }

  @Test
  public void sayHello_someName_rtnHelloName() throws Exception {
    this.mockMvc.perform(get("/hello?name=Amr"))
        .andExpect(status().isOk())
        .andExpect(content().string("Hello Amr"));

  }

}
